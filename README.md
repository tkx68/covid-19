The R script reads a CSV file from the URL <https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv> where data assembled by the Johns Hopkins University is stored.

We visualize confirmed cases by country and calculate the number of days it takes to double the confirmed cases by country. For both we take into account the time intervall for each country in which the confirmed cases are above 100.

![confirmed cases by country](./images/confirmed cases by country.png)

![number of days it takes to double the confirmed](./images/days to double confirmed cases.png)
